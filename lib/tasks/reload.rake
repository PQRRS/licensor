namespace :db do
  desc 'Drop, create, migrate, and seed a database'
  task :reload => :environment do
    pid = rails_process_pid
    if pid!=0
      puts 'Terminating rails server instance...'
      Process.kill 9, pid
    end

    puts "Environment Check: Rails Environment = #{Rails.env}"
    Rake::Task['db:drop'].reenable
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].reenable
    Rake::Task['db:create'].invoke
    Rake::Task['db:migrate'].reenable if Rails.env.development?
    Rake::Task['db:migrate'].invoke if Rails.env.development?
    Rake::Task['db:test:prepare'].reenable if Rails.env.test?
    Rake::Task['db:test:prepare'].invoke if Rails.env.test?
    Rake::Task['db:seed'].reenable
    Rake::Task['db:seed'].invoke

    #puts 'Starting the rails server...'
    #Process.spawn 'rails', 'server'
  end

  desc 'Drop, create, migrate, and seed development and test databases'
  namespace :reload do
    task :all do
      ['development','test'].each do |env|
        Rails.env = env
        puts "=== Starting #{Rails.env} reload ===\n\n"
        Rake::Task['db:reload'].reenable
        Rake::Task['db:reload'].invoke
        puts "=== Finishing #{Rails.env} reload ===\n\n"
      end
    end
  end

  def rails_process_pid
    path = File.join(Rails.root, 'tmp', 'pids', 'server.pid')
    return 0 unless File.exists?(path)
    pid = File.read(path).to_i
    begin
      Process.getpgid pid
    rescue Errno::ESRCH
      pid = 0
    end
    pid
  end
end
