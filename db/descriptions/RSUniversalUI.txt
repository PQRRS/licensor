The **Recognition Suite Universal User Interface** (RS Universal UI) is an easy way to run P.O.S. recognition with a **user-friendly**, yet **powerfull** interface.

It works along with SCLibrary and RELibrary.
