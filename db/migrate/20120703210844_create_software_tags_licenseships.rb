class CreateSoftwareTagsLicenseships < ActiveRecord::Migration
  def change
    create_table :software_tags_licenseships do |t|
      t.references  :license
      t.references  :software_tag
      t.timestamps
    end
  end
end
