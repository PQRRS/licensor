class CreateHardwareTags < ActiveRecord::Migration
  def change
    create_table :hardware_tags do |t|
      t.references  :user
      t.string		  :name
      t.string		  :serial
      # Counter cache columns.
      t.timestamps
    end
  end
end
