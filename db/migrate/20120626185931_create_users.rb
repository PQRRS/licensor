class CreateUsers < ActiveRecord::Migration
  def change
    create_table	:users do |t|
      t.string    :email
      t.string    :company_name
      t.boolean   :is_admin,            default: false
      t.binary	  :password_digest
      t.string    :remember_token
      t.string    :password_reset_token
      t.datetime  :password_reset_sent_at
      # Counter cache columns.
      t.integer   :licenses_count,      default: 0
      t.integer   :hardware_tags_count, default: 0
      t.timestamps
    end
    add_index		  :users, :email, unique: true
    add_index		  :users, :remember_token
  end
end
