class CreateHardwareTagsLicenseships < ActiveRecord::Migration
  def change
    create_table :hardware_tags_licenseships do |t|
      t.references	:license
      t.references	:hardware_tag
    end
  end
end
