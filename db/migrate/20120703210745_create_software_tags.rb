class CreateSoftwareTags < ActiveRecord::Migration
  def change
    create_table :software_tags do |t|
      t.string      :name
      t.string      :identifier
      t.boolean     :is_public
      t.text        :description
      t.timestamps
    end
  end
end
