class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.references  :user
      t.string      :name
      #t.string      :body
      t.integer     :expires_after,       default: Time.now.year
      t.boolean     :is_frozen,           default: false
      t.timestamps
    end
  end
end
