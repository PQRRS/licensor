# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120703210844) do

  create_table "hardware_tags", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "serial"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "hardware_tags_licenseships", :force => true do |t|
    t.integer "license_id"
    t.integer "hardware_tag_id"
  end

  create_table "licenses", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "expires_after", :default => 2014
    t.boolean  "is_frozen",     :default => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "software_tags", :force => true do |t|
    t.string   "name"
    t.string   "identifier"
    t.boolean  "is_public"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "software_tags_licenseships", :force => true do |t|
    t.integer  "license_id"
    t.integer  "software_tag_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "company_name"
    t.boolean  "is_admin",               :default => false
    t.binary   "password_digest"
    t.string   "remember_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.integer  "licenses_count",         :default => 0
    t.integer  "hardware_tags_count",    :default => 0
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

end
