puts 'Creating software tags...'
sc = SoftwareTag.find_or_create_by_identifier!(
  name:                     'Scanner Controller Library',
  identifier:               'fr.PierreQR.RecognitionSuite.SCLibrary.Base',
  is_public:                true,
  description:              File.read(File.join(Rails.root, 'db', 'descriptions', 'SCLibrary.txt'))
)
re = SoftwareTag.find_or_create_by_identifier!(
  name:                     'Recognition Engine Library',
  identifier:               'fr.PierreQR.RecognitionSuite.RELibrary.Base',
  is_public:                true,
  description:              File.read(File.join(Rails.root, 'db', 'descriptions', 'RELibrary.txt'))
)
rsuui = SoftwareTag.find_or_create_by_identifier!(
  name:                     'RS Universal User Interface',
  identifier:               'fr.PierreQR.RecognitionSuite.Gui.RSUniversalUI',
  is_public:                true,
  description:              File.read(File.join(Rails.root, 'db', 'descriptions', 'RSUniversalUI.txt'))
)
rea = SoftwareTag.find_or_create_by_identifier!(
  name:                     'RELibrary Admin',
  identifier:               'fr.PierreQR.RecognitionSuite.Gui.RELibraryAdmin',
  is_public:                false,
  description:              File.read(File.join(Rails.root, 'db', 'descriptions', 'RELibraryAdmin.txt')),
)

puts 'Creating users...'
hickscorp = User.find_or_initialize_by_email({
  email:                    'HicksCorp@GMail.com',
  password:                 'ATMega328',
  password_confirmation:    'ATMega328',
  company_name:             'Pierre qui Roule EURL',
  is_admin:                 false,
  bypass_humanizer:         true
}, without_protection: true)
hickscorp.is_admin          = true
hickscorp.save!

unless Rails.env.production?
  puts 'Creating hardware tags...'
  hickscorp.hardware_tags.build(
    name:                     'Windows XP Pro Dev VM',
    serial:                   '080027A2A7FF'
  ).save!
  hickscorp.hardware_tags.build(
    name:                     'Panini IDeal',
    serial:                   '1029384756'
  ).save!

  puts 'Creating licenses...'
  hickscorp.licenses.find_or_create_by_name!({
    name:                     'In-House Only',
    software_tags:            [ rea, re, sc ],
    hardware_tags:            hickscorp.hardware_tags,
    expires_after:            Time.now.year+3,
    is_frozen:                false
  }, without_protection:      true)
end
