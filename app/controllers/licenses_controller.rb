class LicensesController < ApplicationController
  include AjaxHelper::Renderer
  before_filter :before_everything
  before_filter :before_index,      only: :index
  before_filter :before_existing,   only: [ :show, :edit, :update, :destroy ]
  before_filter :before_destroy,    only: :destroy

  # Get licenses listing.
  def index
    respond_to do |format|
      format.html { redirect_to signed_in_user, notice: 'Listing licenses isn\'t allowed for HTML format.' }
      format.js { render_ajax other: @user }
      format.json { render json: @licenses }
    end
  end

  # Show a single license.
  def show
    respond_to do |format|
      format.html { redirect_to edit_user_license_path(@license.user, @license) }
      format.json { render json: @license }
      format.lic {
        unless admin_signed_in?
          puts "Freezing license #{@license}..."
          @license.is_frozen = true
        end
        @license.save!
        send_data @license.as_text,
          type:         'text/license; charset=UTF-8;',
          disposition:  "attachment; filename=\"#{@license.name}.lic\""
      }
    end
  end

  # License creation form.
  def new
    session[:return_path] = request.referer
    @license = @user.licenses.build
    respond_to do |format|
      format.html
      format.js { render_ajax other: @user }
      format.json { render json: @license }
    end
  end
  # License creation logic.
  def create
    @license = @user.licenses.build
    @license.update_attributes params[:license]
    respond_to do |format|
      if @license.save
        format.html { redirect_to session[:return_path], notice: 'License was successfully created.' }
        format.json { render json: @license, status: :created, location: @license }
      else
        format.html { render action: 'new' }
        format.json { render json: @license.errors, status: :unprocessable_entity }
      end
    end
  end

  # License edition form.
  def edit
    session[:return_path] = request.referer
    respond_to do |format|
      format.html
      format.json { render json: @license }
    end
  end
  # License edition logic.
  def update
    respond_to do |format|
      if @license.update_attributes params[:license]
        format.html { redirect_to session[:return_path], notice: 'License was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @license.errors, status: :unprocessable_entity }
      end
    end
  end

  # License deletion logic.
  def destroy
    @license.destroy
    respond_to do |format|
      format.html { redirect_to user_path(@user), action: :index, notice: 'License was successfully destroyed.' }
      format.js { render_ajax other: @user }
      format.json { head :no_content }
    end
  end

  private
    # Store the user for the licensing request.
    def before_everything
      store_location and redirect_to signin_path, notice: 'Please sign in.' and return unless signed_in?
      # Store requested user.
      uid = params[:user_id].to_i
      @user = signed_in_user.id==uid ? signed_in_user : User.includes([ :licenses, :hardware_tags ]).find(uid)
      # Redirect if unauthorized.
      redirect_to root_path, notice: 'No license was found matching your request.' unless signed_in_as?(@user) || admin_signed_in?
    end
    # Just store licenses belonging to current user.
    def before_index
      @licenses = @user.licenses
    end
    # Stores requested license.
    def before_existing
      @license = @user.licenses.find params[:id]
      redirect_to signed_in_user, notice: 'No license was found matching your request.' unless @user.licenses.include?(@license)
    end
    # Disallow destroying a license unless current user is admin.
    def before_destroy
      redirect_to signed_in_user, notice: 'You are not allowed to delete licenses.' unless admin_signed_in?
    end

end
