class SessionsController < ApplicationController
  # Get current session if any.
  def my
    if signed_in?
      respond_to do |format|
        format.json { render json: signed_in_user }
      end
    else
      respond_to do |format|
        format.html { redirect_to new_session_path }
        format.json { render json: { message: 'Unauthorized.' }, status: :unauthorized }
      end
    end
  end
  def new
  end
  def create
    # Locate user with it's email.
    user = User.find_by_email(params[:session][:email].downcase)
    # Authenticate the user if any.
    if user && user.authenticate(params[:session][:password])
      sign_in user, params[:remember_me]
      redirect_back_or user_path user
    # Authentication error.
    else
      flash[:error] = 'Invalid email / password combination'
      render 'new'
    end
  end
  def destroy
    # End of session.
    sign_out
    redirect_to root_path, flash: { success: 'You were signed out from the application.' }
  end

end
