class HardwareTagsController < ApplicationController
  include AjaxHelper::Renderer
  before_filter :before_everything
  before_filter :before_index,      only: :index
  before_filter :before_existing,   only: [ :show, :edit, :update, :destroy ]
  before_filter :before_destroy,    only: :destroy

  # Get hardware tags listing.
  def index
    respond_to do |format|
      format.html { redirect_to signed_in_user, notice: 'Listing hardware tags isn\'t allowed for HTML format.' }
      format.js { render_ajax other: @user }
      format.json { render :json => @hardware_tags.map(&:attributes) }
    end
  end
  # Show a single hardware tag.
  def show
    respond_to do |format|
      format.html { redirect_to edit_user_hardware_tag_path(@hardware_tag.user, @hardware_tag) }
      format.json { render json: @hardware_tag }
    end
  end

  # Hardware tag creation form.
  def new
    session[:return_path] = request.referer
    @hardware_tag = @user.hardware_tags.build
    respond_to do |format|
      format.html
      format.js { render_ajax other: { user: @user } }
      format.json { render json: @hardware_tag }
    end
  end
  # Hardware tag creation logic.
  def create
    @hardware_tag = @user.hardware_tags.build params[:hardware_tag]
    respond_to do |format|
      if @hardware_tag.save
        format.html { redirect_to session[:return_path], notice: 'Hardware tag was successfully created.' }
        format.js { render_ajax other: { user: @user } }
        format.json { render json: @hardware_tag, status: :created, location: @hardware_tag }
      else
        format.html { render action: :new }
        format.js { render_ajax action: :new, other: { user: @user } }
        format.json { render json: @hardware_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # Hardware tag edition form.
  def edit
    session[:return_path] = request.referer
    respond_to do |format|
      format.html
      format.js { render_ajax other: { user: @user } }
      format.json { render json: @hardware_tag }
    end
  end
  # Hardware tag edition logic.
  def update
    respond_to do |format|
      if @hardware_tag.update_attributes(params[:hardware_tag]) && @hardware_tag.errors.empty?
        format.html { redirect_to (session[:return_path] || signed_in_user), notice: 'Hardware tag was successfully created.' }
        format.js { render_ajax other: { user: @user } }
        format.json { head :no_content }
      else
        format.html { render action: :edit }
        format.js { render_ajax action: :edit, other: { user: @user } }
        format.json { render json: @hardware_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # Hardware tag deletion logic.
  def destroy
    @hardware_tag.destroy
    respond_to do |format|
      format.html { redirect_to user_path(@user), action: :index, notice: 'Hardware tag was successfully destroyed.' }
      format.js { render_ajax }
      format.json { head :no_content }
    end
  end

  private
    # Filter user if not signed in, and stores requested user or authenticated user in @user.
    def before_everything
      store_location and redirect_to signin_path, notice: 'Please sign in.' and return unless signed_in?
      # Store requested user.
      uid = params[:user_id].to_i
      @user = signed_in_user.id==uid ? signed_in_user : User.includes([ :licenses, :hardware_tags ]).find(uid)
      # Redirect if unauthorized.
      redirect_to root_path, notice: 'No hardware tag was found matching your request.' and return unless signed_in_as?(@user) || admin_signed_in?
    end

    # Just store hardware tags belonging to current user.
    def before_index
      @hardware_tags = @user.hardware_tags
      @hardware_tags = @user.hardware_tags.where('LOWER(name) LIKE ?', "#{params[:search].downcase}%")  if params[:search]
    end
    # Stores requested hardware tag.
    def before_existing
      @hardware_tag = @user.hardware_tags.find params[:id]
      redirect_to signed_in_user, notice: 'No hardware tag was found matching your request.' unless @hardware_tag.user_id==@user.id
    end
    # Forbid deletion unless user is admin.
    def before_destroy
      redirect_to signed_in_user, notice: 'You are not allowed to delete hardware tags.' unless admin_signed_in?
    end
end
