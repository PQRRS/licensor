class UsersController < ApplicationController
  include AjaxHelper::Renderer
  before_filter :before_everything,     except: [ :new, :create ]
  before_filter :before_index,          only:   :index
  before_filter :before_new,            only:   [ :new, :create ]
  before_filter :before_existing,       only:   [ :show, :edit, :update, :destroy ]
  before_filter :before_destroy,        only:   :destroy

  # Get users listing.
  def index
    respond_to do |format|
      format.html
      format.js { render_ajax }
      format.json { render json: @users }
    end
  end

  # Shows a single user.
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # User creation form.
  def new
    session[:return_path] = request.referer
    @user = User.new
    respond_to do |format|
      format.html
      format.js { render_ajax }
      format.json { render json: @user }
    end
  end
  # User creation logic.
  def create
    params[:user][:password] = params[:user][:password_confirmation] = "#{SecureRandom.hex(5).upcase}#{SecureRandom.hex(5).downcase}" unless admin_signed_in?
    @user = User.new params[:user]
    @user.bypass_humanizer  = admin_signed_in?
    respond_to do |format|
      if @user.save
        @user.send_account_creation unless admin_signed_in?
        format.html { redirect_to root_path, notice: 'You should have received an email with directions on how to finalize your account creation.' }
        format.js { render_ajax }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.js { render_ajax action: :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # User edition form.
  def edit
    session[:return_path] = request.referer
    respond_to do |format|
      format.html
      format.js { render_ajax }
      format.json { render json: @user }
    end
  end
  # User update logic.
  def update
    respond_to do |format|
      if @user.update_attributes(params[:user])
        sign_in @user
        flash[:success] = 'Your profile was updated.'
        format.html { redirect_to session[:return_path] }
        format.js { render_ajax }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.js { render_ajax action: :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # User deletion.
  def destroy
    @user.destroy
    respond_to do |format|
      flash[:success] = 'Account was deleted.'
      format.html { redirect_to users_url, action: :index, success: 'Account was deleted.' }
      format.js { render_ajax }
      format.json { head :no_content }
    end
  end

  private
    # Filter user if not signed in, and stores requested user or authenticated user in @user.
    def before_everything
      store_location and redirect_to signin_path, notice: 'Please sign in.' and return unless signed_in?
    end

    # Deny index to non-admin.
    def before_index
      return redirect_to signed_in_user unless admin_signed_in?
      @users = User.all
    end
    # Deny user creation to non-admin.
    def before_new
      redirect_to (signed_in? ? signed_in_user : root_path), notice: 'Subscriptions are closed.' unless admin_signed_in?
    end
    # Filter user if not viewing current, or if not admin.
    def before_existing
      # Store requested user.
      uid = params[:id].to_i
      @user = signed_in_user.id==uid ? signed_in_user : User.includes([ :licenses, :hardware_tags ]).find(uid)
      redirect_to signed_in_user, notice: 'You are not allowed to access other user\'s profile.' unless signed_in_as?(@user) || admin_signed_in?
    end
    # Destroying should only be done by admin, and they may not delete themselve.
    def before_destroy
      redirect_to signed_in_user, notice: 'You are not allowed to delete users.' unless admin_signed_in?
    end

end
