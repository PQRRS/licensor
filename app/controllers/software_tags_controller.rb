class SoftwareTagsController < ApplicationController
  include AjaxHelper::Renderer
  before_filter :before_everything, except: [ :index, :show ]
  before_filter :before_index,      only:   :index
  before_filter :before_existing,   only:   [ :show, :edit, :update, :destroy ]
  before_filter :before_altering,   only:   [ :edit, :update, :destroy ]
  before_filter :before_destroy,    only:   :destroy

  # Get software tags listing.
  def index
    respond_to do |format|
      format.html
      format.js { render_ajax }
      format.json { render :json => @software_tags }
    end
  end
  # Show a single software tag.
  def show
    respond_to do |format|
      format.html
      format.json { render json: @software_tag }
    end
  end
  # Software tag creation form.
  def new
    @software_tag = SoftwareTag.new
    respond_to do |format|
      format.html
      format.js { render_ajax }
      format.json { render json: @software_tag }
    end
  end
  # Software tag creation logic.
  def create
    @software_tag = SoftwareTag.new params[:software_tag]
    respond_to do |format|
      if @software_tag.save
        format.html { redirect_to software_tags_path, notice: 'Software tag was successfully created.' }
        format.js { render_ajax }
        format.json { render json: @software_tag, status: :created, location: @software_tag }
      else
        format.html { render action: :new }
        format.js { render_ajax action: :new }
        format.json { render json: @software_tag.errors, status: :unprocessable_entity }
      end
    end
  end
  # Software tag edition form.
  def edit
    respond_to do |format|
      format.html
      format.js { render_ajax }
      format.json { render json: @software_tag }
    end
  end
  # Software tag edition logic.
  def update
    respond_to do |format|
      if @software_tag.update_attributes(params[:software_tag])
        format.html { redirect_to software_tags_path, notice: 'Software tag was successfully updated.' }
        format.js { render_ajax }
        format.json { head :no_content }
      else
        format.html { render action: :edit }
        format.js { render_ajax action: :edit }
        format.json { render json: @software_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # Software tag deletion logic.
  def destroy
    @software_tag.destroy
    respond_to do |format|
      format.html { redirect_to software_tags_path, action: :index, notice: 'Software tag was successfully deleted.' }
      format.js { render_ajax }
      format.json { head :no_content }
    end
  end

  private
    # Filter user if not signed in.
    def before_everything
      store_location and redirect_to signin_path, notice: 'Please sign in.' and return unless signed_in?
    end
    # Just store hardware tags belonging to current user.
    def before_index
      @software_tags = SoftwareTag.scoped
      @software_tags = @software_tags.where('is_public = ?', true) unless admin_signed_in?
      @software_tags = @software_tags.where('LOWER(name) LIKE ?', "#{params[:search].downcase}%") if params[:search]
    end
    # Stores requested hardware tag.
    def before_existing
      @software_tag = (admin_signed_in? ? SoftwareTag : SoftwareTag.where('is_public = ?', true)).find params[:id]
    end
    # Makes sure user is admin.
    def before_altering
      redirect_to signed_in_user, notice: 'No software tag was found matching your request.' unless admin_signed_in?
    end
    def before_destroy
      redirect_to signed_in_user, notice: 'You are not allowed to delete software tags.' unless admin_signed_in?
    end

end
