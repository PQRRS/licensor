class PasswordResetsController < ApplicationController
  before_filter   :before_existing,   only: [ :edit, :update ]
  def new
  end
  def create
    @user.send_password_reset if @user = User.find_by_email(params[:email])
    redirect_to root_path, notice: 'An email containing directions to reset your password has been sent.'
  end

  def edit
  end
  def update
    if @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, alert: 'Password reset has expired.'
    elsif @user.update_attributes params[:user]
      sign_in @user
      redirect_to root_url, notice: 'Password has been reset!'
    else
      render :edit
    end
  end

  private
    # Before existing object filter.
    def before_existing
      @user = User.find_by_password_reset_token! params[:id]
    end
end
