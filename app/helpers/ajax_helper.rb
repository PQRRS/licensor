module AjaxHelper
  module Renderer
    def render_ajax (locals={})
      locals[:object_name]  ||= controller_name.sub('Controller', '').singularize
      locals[:object]       ||= instance_variable_get "@#{locals[:object_name]}"
      locals[:singular]     ||= locals[:object].class.name.underscore
      locals[:plural]       ||= locals[:singular].pluralize
      locals[:action]       ||= action_name
      locals[:other]        ||= {}
      render partial: "shared/ajax/ajax_#{locals[:action]}", locals: locals
    end
  end
end
