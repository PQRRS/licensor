module SessionsHelper
  # Signs-in a user.
  def sign_in (user, remember=false)
    if remember
      cookies.permanent[:remember_token] = user.remember_token
    else
      cookies[:remember_token] = user.remember_token
    end
    self.signed_in_user = user
  end
  # Signs-out the currently signed-in user.
  def sign_out
    self.signed_in_user = nil
    cookies.delete :remember_token
  end
  # Flag to know wether a user is signed in.
  def signed_in?
    !signed_in_user.nil?
  end
  # Tests signed-in user.
  def signed_in_as? (user)
    return signed_in? && signed_in_user==user
  end
  # Flag to know wether an admin is signed in.
  def admin_signed_in?
    signed_in? && signed_in_user.is_admin?
  end

  # Returns the currently signed-in user.
  def signed_in_user
    @signed_in_user ||= User.find_by_remember_token(cookies[:remember_token]) if cookies[:remember_token]
  end
  # Current user assignation.
  def signed_in_user= (user)
    @signed_in_user = user
  end
  # Checks wether the current user is the one in params.
  def signed_in_user? (user)
    @signed_in_user==user
  end

  # Store current location for later redirection.
  def store_location
    session[:return_path] = request.fullpath
  end
  # Redirects to the last stored location, or to the default in params.
  def redirect_back_or (default)
    redirect_to (session[:return_path] || default)
    session.delete :return_path
  end
end
