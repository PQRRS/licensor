require 'creole'

module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title (page_title)
    base_title = 'PQRRS Licensing Server'
    page_title.empty? ? base_title : "#{base_title} | #{page_title}"
  end

  def formated_email_for (usr)
    "#{usr.company_name} <#{usr.email}>"
  end
  def formated_email_link_for (usr)
    return "<a href=\"mailto:#{formated_email_for(usr)}\">#{usr.company_name}</a>"
  end

  def controller? (ctrlr)
    ctrlr.include? params[:controller]
  end
  def action? (*act)
    act.include? params[:action]
  end

  def nav_link (txt, path)
    class_name = current_page?(path) ? 'active' : ''
    content_tag :li, :class => class_name do
      link_to txt, path
    end
  end

  def markdown (text)
    Creole.creolize(text)
  end

  def button_text_for (ctrlr)
      case ctrlr.class.name
        when UsersController.name
          ctrlr.action_name=='edit' ? 'Update Profile' : 'Sign Up!'
        when SessionsController.name
          'Sign in'
        when
          act = ctrlr.action_name=='new' ? 'Create' : 'Update'
          "#{ act } #{ctrlr.object.name}"
      end
  end
end
