replaceContent(
  '<%= dom_id object %>',
  '<%= escape_javascript(render(partial: "#{plural}/#{singular}", locals: { singular.to_sym => object }.merge(other) )) %>'
)
hideModalEditor()
