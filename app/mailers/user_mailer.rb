class UserMailer < ActionMailer::Base
  default from: "NO-REPLY@PierreQR.fr"

  def password_reset (user)
    @user = user
    mail to: user.email, subject: 'PQR Licensing System Password Reset'
  end

  def account_creation (user)
    @user = user
    mail to: user.email, subject: 'PQR Licensing System Account Creation'
  end
end
