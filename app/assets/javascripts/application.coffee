//= require 'jquery'
//= require 'jquery_ujs'
//= require 'jquery.tokeninput'
//= require 'bootstrap'

# Makes an input tokenizable.
window.tokenInputize = (inputName) ->
  if (!$(inputName).length)
    return undefined
  $(inputName).tokenInput($(inputName).data('url'), {
    preventDuplicates:  true
    hintText:           ''
    queryParam:         'search'
    prePopulate:        $(inputName).data 'pre'
  })

# Append
window.appendContentToParent = (parent, content) ->
  $(content).hide().appendTo("##{parent}").fadeIn()
# Replace tag content.
window.replaceContent = (tag, content) ->
  $("##{tag}").replaceWith(content)
# Remove a given tag.
window.remove = (tag) ->
  $("##{tag}").slideUp 300, -> $(this).remove()

# Show modal editor.
window.showModalEditorWithContent = (content) ->
  $("#modal-editor-body").html content
  $("#modal-editor").modal('show')
# Hide modal editor.
window.hideModalEditor = ->
  $("#modal-editor").modal('hide')

$ ->
  tokenInputize '#license_software_tag_tokens'
  tokenInputize '#license_hardware_tag_tokens'
