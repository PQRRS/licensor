class User < ActiveRecord::Base
  include Humanizer

  # Password management stuff.
  has_secure_password

  # Regular accessors.
  attr_accessible       :email, :company_name, :password, :password_confirmation
  attr_accessible       :humanizer_answer, :humanizer_question_id

  def as_json (opts=nil)
    super (opts||{}).merge( { only: [ :email, :company_name, :is_admin ] } )
  end

  attr_accessor         :bypass_humanizer
  require_human_on      :create,
                          unless:       :bypass_humanizer

  # Relationships.
  has_many              :licenses,
                          dependent:    :destroy
  has_many              :software_tags,
                          through:      :licenses
  has_many              :hardware_tags,
                          dependent:    :destroy

  # eMail validation.
  VALID_EMAIL_REGEX     = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates             :email,
                          uniqueness:   { case_sensitive: false },
                          format:       { with: VALID_EMAIL_REGEX, message: 'must be a valid email address' }
  # Company name validation.
  validates             :company_name,
                          length:       { minimum: 5 },
                          uniqueness:   { case_sensitive: false }
  # Password validation.
  VALID_PASSWORD_REGEX  = /^(?=.*\d)(?=.*([a-z]|[A-Z]))([\x20-\x7E]){5,40}$/
  validates             :password,
                          format:       {
                            on:       :create,
                            with:     VALID_PASSWORD_REGEX,
                            message:  'must contain at least 5 characters including one lowercase letter and one uppercase letter as well as a digit'
                          }
  validates             :password_confirmation,
                          on:         :create,
                          presence:   true

  before_create         { generate_token_in :remember_token }
  before_save           { self.email.downcase! }

  def is_confirmed?
    created_at!=password_reset_sent_at
  end

  def send_password_reset
    generate_token_in :password_reset_token
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def send_account_creation
    generate_token_in :password_reset_token
    self.password_reset_sent_at = created_at
    save!
    UserMailer.account_creation(self).deliver
  end

  def generate_token_in col
    begin
      self[col] = SecureRandom.urlsafe_base64
    end while User.exists? col => self[col]
  end
end
