require 'digest/md5'

class License < ActiveRecord::Base
  attr_accessible   :user_id, :name, :software_tag_tokens, :hardware_tag_tokens, :expires_after, :is_frozen

  belongs_to        :user,
                      counter_cache:  true

  has_many          :software_tags_licenseships,
                      dependent:      :destroy
  has_many          :software_tags,
                      through:        :software_tags_licenseships
  has_many          :hardware_tags_licenseships,
                      dependent:      :destroy
  has_many          :hardware_tags,
                      through:        :hardware_tags_licenseships

  validates         :user_id,
                      presence:       true

  validates         :name,
                      presence:       true,
                      length:         { minimum: 5 },
                      format:         { with: /^[\w\s\-\_]*$/u, message: 'can only contain alphanumerical characters, spaces, dashes and underscores.' }

  validates         :software_tags,
                      with:           :validate_software_tags
  validates         :hardware_tags,
                      with:           :validate_hardware_tags
  validates         :expires_after,
                      with:           :validate_expires_after

  CK                = [ 0x821bb6c8, 0x2d0bced0, 0x9a25ebdd, 0x06d11fe0 ]

  # Additional accessors are required for the software tags token fields to work.
  attr_reader       :software_tag_tokens
  def software_tag_tokens= (ids)
    ids = ids.split(',').uniq.sort
    # Store previously assigned software tags for validation checks if license is frozen.
    @software_tag_ids_was = self.software_tag_ids if is_frozen?
    self.software_tag_ids = ids
  end
  # When updating the software tags, make sure none of them are being removed if the license is frozen.
  def validate_software_tags
    @software_tag_ids_was ||= []
    return unless is_frozen? && (self.software_tag_ids & @software_tag_ids_was)!=@software_tag_ids_was
    errors[:software_tag_tokens] = 'cannot be removed since this license is frozen.'
  end

  # Additional accessors are required for the hardware tags token fields to work.
  attr_reader       :hardware_tag_tokens
  def hardware_tag_tokens= (ids)
    ids   = ids.split(',').uniq.sort
    ids.delete_if { |id| HardwareTag.find(id).user!=user }
    # Store previously assigned hardware tags for validation checks if license is frozen.
    @hardware_tag_ids_was = self.hardware_tag_ids if is_frozen?
    self.hardware_tag_ids = ids
  end
  # When updating the hardware tags, make sure none of them are being removed if the license is frozen.
  def validate_hardware_tags
    @hardware_tag_ids_was ||= []
    return unless is_frozen? && (self.hardware_tag_ids & @hardware_tag_ids_was)!=@hardware_tag_ids_was
    errors[:hardware_tag_tokens] = 'cannot be removed since this license is frozen.'
  end

  def fancy_id
     "#{created_at.strftime '%Y%m'}#{"%05d" % id}"
  end

  def validate_expires_after
    return unless is_frozen? && expires_after<expires_after_was
    errors[:expires_after] = 'cannot be lower since this license is frozen.'
  end

  def as_text (opts={})
    sunKey              = sun
    moonKey             = moon sunKey
    equinoxKey          = equinox sunKey, moonKey
    puts                check_keys(sunKey, moonKey, equinoxKey) ? "Oh yeah." : "Bummer..."
    <<-RUBY
      License Name:     #{fancy_id} #{name}
      Registered Name:  #{user.company_name}
      Registered Email: #{user.email}
      Useable Software: #{software_tags.collect{|s| s.identifier.downcase }.sort * ', '}
      Useable Hardware: #{hardware_tags.collect{|s| s.serial.downcase }.sort * ', '}
      Expires After:    #{expires_after}
      Moon Key:         #{moonKey}
      Equinox Key:      #{equinoxKey}
    RUBY
  end

  # Generate a license key for the current license model instance.
  def sun
    sth       = software_tags.collect { |st| st.identifier.downcase }.sort * ':float:'
    hth       = hardware_tags.collect { |ht| ht.serial.downcase }.sort * ':uint32:'
    h         = "bof:#{hth}:string:#{sth}:sqlite3:#{expires_after-2000}:db:#{fancy_id} #{name.downcase}:fmt:#{user.email.downcase}:spec:#{user.company_name.downcase}:eof"
    Digest::MD5.hexdigest(h).scan(/.{8}/).join('-')
  end
  # Generate a validation key for the current license model instance.
  def moon (s)
    sr      = s.delete('-')
    Digest::MD5.hexdigest("#{sr}__vtable__#{sr.reverse}").scan(/.{8}/).join('-')
  end
  # Generate equinox key for a sun and a moon key.
  def equinox (s, m)
    sBlks, mBlks  = [ s, m ].collect { |sz| sz.split '-' }
    (0...(sBlks.size)).collect { |i|
      blks          = [ sBlks[i].to_i(16), mBlks[i].to_i(16) ].sort
      (blks[1] ^ blks[0] ^ CK[i]).to_s(16).rjust(8, '0')
    }.join('-')
  end
  # Check 3 keys, try to get cheesecake back.
  def check_keys (s, m, e)
    sBlks, mBlks, eBlks  = [ s, m, e ].collect { |sz| sz.split '-' }
    CK == (0...(sBlks.size)).collect { |i|
      blks          = [ sBlks[i].to_i(16), mBlks[i].to_i(16) ].sort
      blks[1] ^ blks[0] ^ eBlks[i].to_i(16)
    }
  end
end
