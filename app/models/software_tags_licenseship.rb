class SoftwareTagsLicenseship < ActiveRecord::Base
  belongs_to    :license
  belongs_to    :software_tag
end
