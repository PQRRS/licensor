class SoftwareTag < ActiveRecord::Base
  attr_accessible  :name, :identifier, :is_public, :description

  has_many    :software_tags_licenseships
  has_many    :licenses,
                through:      :software_tags_licenseships
  has_many    :hardware_tags,
                through:      :licenses
  has_many    :users,
                through:      :licenses

  validates   :name,
                length:       { minimum: 5 }

  validates   :identifier,
                uniqueness:   true,
                length:       { minimum: 5 },
                format:       { with:   /^[\w.]*$/u, message: 'can only contain alphanumerical characters, dots and spaces.' }

  # Override to_json so the body is hidden.
  def as_json(opts={})
    opts[:only] ||= [ :id, :name ]
    super opts
  end

end
