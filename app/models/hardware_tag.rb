class HardwareTag < ActiveRecord::Base
  attr_accessible 	:user_id, :name, :serial

  belongs_to        :user,
                      counter_cache:  true
  has_many          :hardware_tags_licenseships
  has_many          :licenses,
                      through:        :hardware_tags_licenseships

  validates         :user_id,
                      presence:       true

  validates         :name,
                      length:         { minimum: 5 }

  before_validation :cleanup_serial
  validates         :serial,
                      uniqueness:     { scope: :user_id},
                      with:           :validate_serial

  # Cleans a serial number by removing any non-hex digit.
  def cleanup_serial
    self.serial = serial.scan(/[[:xdigit:]]/).join.downcase if serial
  end
  # If a serial is changed while the hardware tag is frozen, reject.
  def validate_serial
    if self.is_frozen? && self.serial_changed?
      errors[:serial] = 'cannot be edited since this hardware tag is in use by a frozen license.'
    end
  end
  # Tells wether this hardware tag is included by a frozen license.
  def is_frozen?
    self.licenses.find_all_by_is_frozen(true).size!=0
  end
end
