FactoryGirl.define do
  # Create sequences we'll be using for users.
  sequence(:email)          { |n| "foo#{n}@example.com" }
  sequence (:company_name)  { |n| "#{n} Inc. LLC. Corp." }

  # Generate a user.
  factory :user do
    bypass_humanizer      true
    company_name
    email
    password              'Secret01'
    password_confirmation 'Secret01'
    is_admin              false
  end
end
