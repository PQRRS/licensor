require 'spec_helper'

describe UserMailer do
  describe 'password_reset' do
    let(:user) { FactoryGirl.create(:user, password_reset_token: 'Anything!') }
    let(:mail) { UserMailer.password_reset(user) }

    it 'sends user password reset url' do
      mail.from.should          eq(['NO-REPLY@PierreQR.fr'])
      mail.to.should            eq([user.email])
      mail.subject.should       eq('PQR Licensing System Password Reset')
      mail.body.encoded.should  match(edit_password_reset_path(user.password_reset_token))
    end
  end
end
