require 'spec_helper'

describe 'PasswordResets' do
  it 'emails user when requesting password reset' do
    user = FactoryGirl.create :user
    visit signin_path
    click_link 'Reset Password'
    fill_in 'Email', with: user.email
    click_button 'Reset Password'
    current_path.should eq(root_path)
    page.should have_content 'An email containing directions to reset your password has been sent.'
    last_email.to.should include(user.email)
  end

  it 'does not email an invalid user when requesting password reset' do
    visit signin_path
    click_link 'Reset Password'
    fill_in 'Email', with: 'nobody@nowhere.com'
    click_button 'Reset Password'
    current_path.should eq(root_path)
    page.should have_content 'An email containing directions to reset your password has been sent.'
    last_email.should be_nil
  end
end
