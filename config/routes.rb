PQRLicensor::Application.routes.draw do
  # Default landing page.
  root                          to: 'static_pages#home'
  # Rewriting of statics.
  match       '/home',          to: 'static_pages#home'
  match       '/how_it_works',  to: 'static_pages#how_it_works'
  # Rewriting of sign-up / sign-in.
  match       '/signup',        to: 'users#new'
  match       '/signin',        to: 'sessions#new'
  match       '/signout',       to: 'sessions#destroy', via: :delete
  match       '/sessions/my',   to: 'sessions#my'
  resources   :sessions,        only: [ :new, :my, :create, :destroy ]
  # Password resets...
  get         'password_resets/new'
  resources   :password_resets
  # Resources access.
  resources   :sessions,  only: [ :new, :create, :destroy ]
  resources   :users do
    resources   :hardware_tags
    resources   :licenses
  end
  resources   :software_tags
end
