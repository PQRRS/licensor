PQRLicensor::Application.configure do
  config.cache_classes                            = true
  config.assets.compress                          = true
  config.assets.debug                             = false
  config.whiny_nils                               = true
  config.active_support.deprecation               = :notify
  config.consider_all_requests_local              = false
  config.action_controller.perform_caching        = true
  config.serve_static_assets                      = false
  config.assets.compile                           = true
  config.assets.digest                            = true
  config.i18n.fallbacks                           = true
  # Mailer configuration.
  ActionMailer::Base.delivery_method              = :smtp
  ActionMailer::Base.perform_deliveries           = true
  ActionMailer::Base.raise_delivery_errors        = true
  ActionMailer::Base.smtp_settings                = {
      address:              'smtp.gmail.com',
      port:                 '587',
      domain:               'gmail.com',
      user_name:            'HicksCorp@GMail.com',
      password:             'ATMega328',
      authentication:       'plain',
      enable_starttls_auto: true
  }
  config.action_mailer.default_url_options        = { host: 'Licensor.PierreQR.fr' }
end
