PQRLicensor::Application.configure do
  config.cache_classes                            = false
  config.assets.compress                          = false
  config.assets.debug                             = true
  config.whiny_nils                               = true
  config.consider_all_requests_local              = true
  config.action_controller.perform_caching        = false
  config.action_mailer.raise_delivery_errors      = false
  config.active_support.deprecation               = :log
  config.action_dispatch.best_standards_support   = :builtin
  config.active_record.mass_assignment_sanitizer  = :strict
  config.active_record.auto_explain_threshold_in_seconds = 0.5
  # Mailer configuration.
  ActionMailer::Base.delivery_method              = :smtp
  ActionMailer::Base.perform_deliveries           = true
  ActionMailer::Base.raise_delivery_errors        = true
  ActionMailer::Base.smtp_settings                = {
      address:              'smtp.gmail.com',
      port:                 '587',
      domain:               'gmail.com',
      user_name:            'HicksCorp@GMail.com',
      password:             'ATMega328',
      authentication:       'plain',
      enable_starttls_auto: true
  }
  config.action_mailer.default_url_options        = { host: 'localhost:3000' }
end
