require 'rvm/capistrano'
require 'bundler/capistrano'
set   :rvm_type,        :system
set   :rvm_path,        '/usr/local/rvm'
#set   :rvm_ruby_string, '1.9.3-p448'

# Main configuration.
server 'VaporLounge.PierreQR.fr', :app, :web, :db, primary: true
set     :application,         'Pierre qui Roule - Recognition Suite Licensor'
set     :keep_releases,       3

# Servers details.
ssh_options[:port]            = '22'
ssh_options[:keys]            = %w('~/.ssh/HicksCorp@GMail.com')
ssh_options[:forward_agent]   = true
default_run_options[:pty]     = true
set   :deploy_to,             '/home/system/passenger/sites/pierreqr.fr/licensor'
set   :deploy_via,            :remote_cache
set   :user,                  'passenger'
set   :use_sudo,              false

# Repository configuration.
set   :scm,                   :git
set   :scm_username,          'passenger'
set   :repository,            'git@VaporLounge.PierreQR.fr:PQR/RecognitionSuite/Licensor.git'
set   :branch,                'master'
set   :git_enable_submodules, 1

# Tasks.
namespace :deploy do
  desc 'Start Application'
  task  :start, roles: :app do
    run   "touch #{current_path}/tmp/restart.txt"
  end

  desc 'Stop Application'
  task  :stop, roles: :app do
  end

  desc 'Restart Application'
  task :restart, roles: :app do
    run "touch #{current_path}/tmp/restart.txt"
  end
end

# Hook to automatically trust .rvmrc.
namespace :rvm do
  desc 'Trust RVM RC'
  task :trust_rvmrc do
    run "rvm rvmrc trust #{current_release}"
  end
end

#before  'deploy',               'deploy:setup'
after   'deploy:update_code',   'rvm:trust_rvmrc'
after   'deploy:update_code',   'deploy:migrate'
after   'deploy:restart',       'deploy:cleanup'
