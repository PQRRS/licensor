# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
PQRLicensor::Application.initialize!

Mime::Type.register 'text/license', :lic
